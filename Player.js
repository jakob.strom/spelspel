exports.Player = function(playerName, ws) {
    this.playerName = playerName;
    this.score = 0;
}

exports.Player.prototype.addScore = function(score) {
    this.score += score;
}
