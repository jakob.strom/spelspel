var express = require('express');
var bodyParser = require('body-parser');
var app = express();
var port = process.env.PORT || 3001;
var expressWs = require('express-ws')(app);
var http = require('http');
var Game = require('./Game.js').Game
var Player = require('./Player.js').Player

var games = {

};

app.use(bodyParser.json()); // for parsing application/json
app.use(express.static(__dirname + '/public'));
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.listen(port);

console.log("Server is live and listening on port " + port);


app.get('/games', function(req, res) {
  res.send(Object.keys(games));
});

app.post('/games', function(req, res) {
    console.log(req.body);

    var gameName = req.body.gameName;
    
    games[gameName] = new Game(gameName);

    res.send();
});

app.get('/games/:name', function(req, res) {
    var game = games[req.params.name];
    res.send(game.players);
});

app.post('/games/:name', function(req, res) {
    var scores = req.body.scores;
    var gameName = req.params.name;

    game[gameName].distributeScores(scores);
});

app.ws('/games/:name', function(ws, req) {
    ws.on('message', function(data) {
        if (data === 'secretgamestartcommand') {
            console.log("started game")
            startGame(req.params.name);
        } else {
            console.log(req.params.name);
            games[req.params.name].addPlayer(new Player(data), ws);
            ws.send(JSON.stringify({players: games[req.params.name].players}));
        }
    });
});
// https://afternoon-everglades-48732.herokuapp.com/game/123/456
function startGame(gameName) {
    var data = JSON.stringify({
        'gameName': gameName,
        'players': games[gameName].getPlayerNames()
    });

    var options = {
        host: 'afternoon-everglades-48732.herokuapp.com',
        port: '80',
        path: '/initGame',
        method: 'POST',
        headers: {
        'Content-Type': 'application/json; charset=utf-8',
        'Content-Length': data.length
        }
    };

    var req = http.request(options, function(res) {
        var msg = '';

        res.setEncoding('utf8');
        res.on('data', function(chunk) {
        msg += chunk;
        });
        res.on('end', function() {
        console.log(" got a response");
        });
    });

    req.write(data);
    req.end();

    notifyGameStartToPlayers(gameName, games[gameName].websockets);
}

function notifyGameStartToPlayers(gameName, websockets) {
    for (var i = 0; i < websockets.length; i += 1) {
        websockets[i].send(JSON.stringify({redirectURL: 'http://afternoon-everglades-48732.herokuapp.com/game/' + gameName + '/'}));
    }
}
