exports.Game = function(gameName, playersMin, playersMax, initGameUrl, playerGameUrl) {
    this.gameName = gameName;
    this.players = [];
    this.playersMin = playersMin;
    this.playersMax = playersMax;
    this.initGameUrl = initGameUrl;
    this.playerGameUrl = playerGameUrl;
    this.websockets = [];
}

exports.Game.prototype.addPlayer = function(player, ws) {
    this.players.push(player);
    this.websockets.push(ws);
    console.log('Added player ' + player.playerName);
}

exports.Game.prototype.getPlayerNames = function() {
    var playerNames = [];
    for (var i = 0; i < this.players.length; i += 1) {
        playerNames.push(this.players[i].playerName);
    }
    return playerNames;
}

exports.Game.prototype.distributeScores = function(playerList) {
    for (var i = 0; i <= playerList.length; i += 1) {
        this.getPlayerByName(playerList[i]).addScore(140 - 20*i);
    }
}

exports.Game.prototype.getPlayerByName = function(name) {
    for (var i = 0; i <= players.length; i += 1) {
        if (players[i].name === name) {
            return players[i];
        }
    }
    return null;
}
